<?php

namespace Drupal\webform_myemma\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Settings Form for MyEmma webform field.
 */
class SettingsForm extends ConfigFormBase implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_myemma_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_myemma.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get configuration settings for MyEmma.
    $config = $this->config('webform_myemma.settings');

    $form_state->setCached(FALSE);

    // Define fieldset and fields for required default account.
    $form['default_account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default Account'),
    ];

    $form['default_account']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MyEmma Account ID'),
      '#default_value' => $config->get('account_id'),
      '#required' => TRUE,
    ];

    $form['default_account']['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MyEmma public key'),
      '#default_value' => $config->get('public_key'),
      '#required' => TRUE,
    ];

    $form['default_account']['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MyEmma private key'),
      '#default_value' => $config->get('private_key'),
      '#required' => TRUE,
    ];

    // Add fields for any alternate accounts.
    if ($accounts = $config->get('accounts')) {
      foreach ($accounts as $a => $account) {
        $form['accounts'][$a]['account_name_' . $a] = [
          '#type' => 'textfield',
          '#disabled' => 'true',
          '#size' => 30,
          '#default_value' => $account['account_name'],
        ];
        $form['accounts'][$a]['account_id_' . $a] = [
          '#type' => 'textfield',
          '#size' => 30,
          '#default_value' => $account['account_id'],
        ];
        $form['accounts'][$a]['public_key_' . $a] = [
          '#type' => 'textfield',
          '#size' => 30,
          '#default_value' => $account['public_key'],
        ];

        $form['accounts'][$a]['private_key_' . $a] = [
          '#type' => 'textfield',
          '#size' => 30,
          '#default_value' => $account['private_key'],
        ];
      }
    }

    // Add fields for defining an additional account.
    $form['accounts']['add']['account_name_add'] = [
      '#type' => 'machine_name',
      '#size' => 30,
      '#machine_name' => [
        'exists' => [$this, 'accountNameExists'],
        'standalone' => TRUE,
      ],
    ];

    $form['accounts']['add']['account_id_add'] = [
      '#type' => 'textfield',
      '#size' => 30,
    ];

    $form['accounts']['add']['public_key_add'] = [
      '#type' => 'textfield',
      '#size' => 30,
    ];

    $form['accounts']['add']['private_key_add'] = [
      '#type' => 'textfield',
      '#size' => 30,
    ];

    $form['additional_accounts'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional Accounts'),
      '#description' => $this->t('Add additional MyEmma accounts to use for Webform Handler.'),
    ];

    // Build frame of table to hold alternate account fields.
    $form['additional_accounts']['accounts_table'] = [
      '#type' => 'table',
      '#rows' => [],
      '#header' => [
        $this->t('Account Machine Name'),
        $this->t('Account ID'),
        $this->t('Public Key'),
        $this->t('Private Key'),
      ],
    ];

    // Add pre_render method to move account fields into a table.
    $form['#pre_render'] = [
      [$this, 'preRenderForm'],
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * Machine name exists callback to check if account machine name is used.
   *
   * @param string $account_name
   *   The submitted account name.
   * @param array $element
   *   The submission element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal Form State object.
   *
   * @return bool
   *   Boolean TRUE if name is already used, else boolean FALSE.
   */
  public function accountNameExists($account_name, array $element, FormStateInterface $form_state) {
    $exists = FALSE;
    // Get configuration object.
    $config = $this->config('webform_myemma.settings');
    // Get configured accounts.
    if ($accounts = $config->get('accounts')) {
      // Check if submitted account anme exists.
      if (isset($accounts[$account_name])) {
        $exists = TRUE;
      }
    }

    return $exists;
  }

  /**
   * {@inheritDoc}
   */
  public function preRenderForm(array $form) {
    // Loop through each alternate account fields and move into table.
    foreach (Element::children($form['accounts']) as $child) {
      // Move account fields from the form to the table in a row.
      $form['additional_accounts']['accounts_table']['#rows'][] = [
        ['data' => $form['accounts'][$child]['account_name_' . $child]],
        ['data' => $form['accounts'][$child]['account_id_' . $child]],
        ['data' => $form['accounts'][$child]['public_key_' . $child]],
        ['data' => $form['accounts'][$child]['private_key_' . $child]],
      ];
      unset($form['accounts'][$child]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /* @var $config \Drupal\Core\Config\Config */
    $config = $this->configFactory->getEditable('webform_myemma.settings');
    // Save default account.
    $config->set('account_id', $form_state->getValue('account_id'))->save();
    $config->set('public_key', $form_state->getValue('public_key'))->save();
    $config->set('private_key', $form_state->getValue('private_key'))->save();

    // Update alternate accounts.
    $accounts = [];
    // Loop through each alternate account.
    foreach (Element::children($form['accounts']) as $account) {
      // Get account values from form fields.
      $account_name = $form_state->getValue('account_name_' . $account);
      $account_id = $form_state->getValue('account_id_' . $account);
      $public_key = $form_state->getValue('public_key_' . $account);
      $private_key = $form_state->getValue('private_key_' . $account);

      // Confirm account has unique name, id and keys before storing.
      if ($account_name && $account_id && $public_key && $private_key && !isset($accounts[$account_name])) {
        $accounts[$account_name] = [
          'account_name' => $account_name,
          'account_id' => $account_id,
          'public_key' => $public_key,
          'private_key' => $private_key,
        ];
      }
    }

    // Save alternate accounts. Accounts could be empty and that is OK.
    $config->set('accounts', $accounts)->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderForm'];
  }

}
