<?php

namespace Drupal\webform_myemma\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MarkRoland\Emma\Client;

/**
 * Form submission to MyEmma handler.
 *
 * @WebformHandler(
 *   id = "myemma",
 *   label = @Translation("MyEmma"),
 *   category = @Translation("MyEmma"),
 *   description = @Translation("Sends a form submission to a MyEmma group."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformMyEmmaHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTranslationManagerInterface
   */
  protected $tokenManager;

  /**
   * The plugin configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $emmaConfig;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('webform_myemma');
    $instance->emmaConfig = $container->get('config.factory')->get('webform_myemma.settings');
    $instance->tokenManager = $container->get('webform.token_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'account_name' => '',
      'email' => '',
      'group_id' => '',
      'fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->applyFormStateToConfiguration($form_state);

    $form['myemma'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('MyEmma settings'),
    ];
    $form['myemma']['group_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group Id'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['group_id'],
      '#description' => $this->t('Group ID (numerical) of the list to add the submission to.'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $account_options = [];
    // Get available accounts.
    if ($emma_accounts = $this->getEmmaAccounts()) {
      // The keys should be the account names.
      $emma_accounts = array_keys($emma_accounts);
      foreach ($emma_accounts as $name) {
        $account_options[$name] = $name;
      }
    }

    $form['myemma']['account_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Account'),
      '#required' => TRUE,
      '#options' => $account_options,
      '#default_value' => $this->configuration['account_name'],
      '#description' => $this->t('The MyEmma account to use for submission.'),
    ];

    $fields = $this->getWebform()->getElementsDecoded();
    $options = [];
    $options[''] = $this->t('- Select an option -');
    foreach ($fields as $field_name => $field) {
      if ($field['#type'] == 'email') {
        $options[$field_name] = $field['#title'];
      }
    }

    $form['myemma']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'],
      '#options' => $options,
    ];

    // Get elements options.
    $element_options = [];
    $elements = $this->webform->getElementsInitializedFlattenedAndHasValue();
    $element_key = [];

    foreach ($elements as $element_key => $element) {
      if ($element_key != 'email') {
        $element_options[$element_key] = (isset($element['#title'])) ? $element['#title'] : $element_key;
      }

    }

    $form['myemma']['fields'] = [
      '#type' => 'webform_mapping',
      '#title' => 'MyEmma Field Shortcut',
      '#description' => $this->t('Shortcut name from MyEmma, contains underscores.'),
      '#description_display' => 'before',
      '#default_value' => $this->configuration['fields'],
      '#source' => $element_options,
    ];

    return $form;
  }
  /**
   * Display form fields for mapping to the MyEmma fields.
   *
   * @param array &$form
   *   The form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function selectLookup(array &$form, FormStateInterface $form_state) {
    $lookup_by = $form_state->getValue('map_to_webform');
    $form = [];
    switch ($lookup_by) {
      case '1':
        $form['myemma']['fields'] = [
          '#type' => 'webform_mapping',
          '#title' => 'My Emma Field Shortcut',
          '#description' => $this->t('Please select which fields webform submission data should be mapped to'),
          '#description_display' => 'before',
          '#default_value' => $this->configuration['fields'],
          '#required' => TRUE,
          '#source' => $element_options,
        ];
        break;
      default:
        break;
      }
    }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values['myemma'][$name])) {
        $this->configuration[$name] = $values['myemma'][$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If update, do nothing
    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);

    // Replace tokens.
    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    $group_ids = [];
    // Check if passed an array of multiple groups.
    if (is_array($configuration['group_id'])) {
      $group_ids = $configuration['group_id'];
    }
    // Check if a string containing multiple groups seperated by commas.
    elseif (strpos($configuration['group_id'], ",")) {
      // Split string into an array on commas.
      $group_ids = explode(",", $configuration['group_id']);
      // Trim array values as there may have been spaces.
      $group_ids = array_map('trim', $group_ids);
    }
    // Else build an array with a single element.
    else {
      $group_ids = [$configuration['group_id']];
    }

    $email = $fields['data'][$configuration['email']];
    $emma_fields = [];
    foreach ($configuration['fields'] as $key => $emma_name) {
      foreach ($fields['data'] as $name => $value) {
        if ($key == $name) {
          $emma_fields[$emma_name] = $fields['data'][$name];
        }
      }
    }

    // Get available accounts.
    $emma_accounts = $this->getEmmaAccounts();
    // Have a valid emma account to use with this handler?
    if (($account_name = $configuration['account_name']) && isset($emma_accounts[$account_name])) {
      // Submit to MyEmma.
      $emma_account = $emma_accounts[$account_name];
      try {
        $emmaClient = new Client($emma_account['account_id'], $emma_account['public_key'], $emma_account['private_key']);

        $response = $emmaClient->import_single_member($email, $emma_fields, $group_ids);

        if (!$response) {
          $this->logger->error('MyEmma settings are incorrect, set at /admin/config/services/webform_myemma and confirm the Group ID is correct in webform handler');
        }
      }
      catch (\Exception $e) {
        $this->logger->error('MyEmma settings not set, set at /admin/config/services/webform_myemma');
      }
    }
    else {
      $this->logger->error(sprintf('MyEmma Account not configured in webform handler for the webform: %s.',
      $webform_submission->getWebform()->get('title')));
    }
  }

  /**
   * Return an array of configured MyEmma accounts.
   *
   * @return array
   *   An associative array of myemma accounts.
   */
  protected function getEmmaAccounts() {
    $accounts = [];

    // Add the default account if it is set.
    if ($default_account_id = $this->emmaConfig->get('account_id')) {
      $accounts['default'] = [
        'account_name' => 'default',
        'account_id' => $default_account_id,
        'public_key' => $this->emmaConfig->get('public_key'),
        'private_key' => $this->emmaConfig->get('private_key'),
      ];
      // Add alternative accounts, if any are stored.
      if ($alt_accounts = $this->emmaConfig->get('accounts')) {
        foreach ($alt_accounts as $name => $account) {
          $accounts[$name] = $account;
        }
      }
    }

    return $accounts;
  }

}
