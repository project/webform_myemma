INTRODUCTION
-------------

The MyEmma for Webform module creates a handler for adding email addresses
submitted through a webform to a MyEmma account.

REQUIREMENTS
------------

Depends on the [Webform](https://www.drupal.org/project/webform) module
and installing with composer to include required library: 
[markroland/emma:^3.0](https://github.com/markroland/emma)

INSTALLATION
------------

Install with composer, `composer require drupal/webform_myemma`. 

CONFIGURATION
-------------

1. Add your MyEmma credentials to the configuration at  /admin/config/services/webform_myemma.
   a. Account and API keys are found in your MyEmma account profile
   b. Add additional accounts that may be selected in the webform handler
2. Add the MyEmma handler to your webform, email address and group ID are required.
   a. Group ID is numeric, found in the address URL when viewing a group in your MyEmma account 
   b. Select the account created in the module configuration that this handler should use should use.
   c. Map the Source field (from the webform) to your MyEmma account using the MyEmma "field shortcut" in the Destination. You can fiend the field shortcut when editing the field in your MyEmma account.
